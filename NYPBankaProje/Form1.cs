﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NYPBankaProje
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        Adres adresBilgileri = new Adres();
        List<Yetkiler> yetkiListesi = new List<Yetkiler>();
        List<KrediTipleri> krediTipleri = new List<KrediTipleri>();
        List<IslemTipleri> islemTipleri = new List<IslemTipleri>();
        List<musteriTip> musteriTipleri = new List<musteriTip>();
        List<HesapTip> hesapTipleri = new List<HesapTip>();

        public void defaultBilgileriDoldur()
        {
            //Yetkiler
            Yetkiler gorevli = new Yetkiler();
            gorevli.yetkiId = 1;
            gorevli.yetkiAdi = "Gişe Görevlisi";
            yetkiListesi.Add(gorevli);
            Yetkiler musteriHizmetleri = new Yetkiler();
            musteriHizmetleri.yetkiId = 2;
            musteriHizmetleri.yetkiAdi = "Müşteri Hizmetleri";
            yetkiListesi.Add(musteriHizmetleri);
            Yetkiler subeSorumlusu = new Yetkiler();
            subeSorumlusu.yetkiId = 3;
            subeSorumlusu.yetkiAdi = "Şube Sorumlusu";
            yetkiListesi.Add(subeSorumlusu);
            Yetkiler subeMuduru = new Yetkiler();
            subeMuduru.yetkiId = 4;
            subeMuduru.yetkiAdi = "Şube Müdürü";
            yetkiListesi.Add(subeMuduru);
            Yetkiler bolgeBaskani = new Yetkiler();
            bolgeBaskani.yetkiId = 5;
            bolgeBaskani.yetkiAdi = "Bölge Başkanı";
            yetkiListesi.Add(bolgeBaskani);
            
            //Adres
            sehirlerComboBx.DisplayMember = "IlAdi";
            sehirlerComboBx.ValueMember = "IlId";
            sehirlerComboBx.DataSource = adresBilgileri.illeriDondur();

            //İşlem Tipleri
            IslemTipleri Havale = new IslemTipleri();
            Havale.islemTipId = 1;
            Havale.islemTipAdi = "Havale";
            islemTipleri.Add(Havale);
            IslemTipleri paraCekme = new IslemTipleri();
            paraCekme.islemTipId = 2;
            paraCekme.islemTipAdi = "Para Çekme";
            islemTipleri.Add(paraCekme);
            IslemTipleri paraYatirma = new IslemTipleri();
            paraYatirma.islemTipId = 3;
            paraYatirma.islemTipAdi = "Para Yatırma";
            islemTipleri.Add(paraYatirma);
            IslemTipleri Doviz = new IslemTipleri();
            Doviz.islemTipId = 4;
            Doviz.islemTipAdi = "Döviz";
            islemTipleri.Add(Doviz);

            //Kredi Tipi
            KrediTipleri kisaVadeli = new KrediTipleri();
            kisaVadeli.krediTipId = 1;
            kisaVadeli.krediTipAdi = "Kısa Vadeli Kredi";
            krediTipleri.Add(kisaVadeli);
            KrediTipleri ortaVadeli = new KrediTipleri();
            ortaVadeli.krediTipId = 2;
            ortaVadeli.krediTipAdi = "Orta Vadeli Kredi";
            krediTipleri.Add(ortaVadeli);
            KrediTipleri uzunVadeli = new KrediTipleri();
            uzunVadeli.krediTipId = 3;
            uzunVadeli.krediTipAdi = "Uzun Vadeli Kredi";
            krediTipleri.Add(uzunVadeli);

            //Müşteri Tipi
            musteriTip Bireysel = new musteriTip();
            Bireysel.musteriTipi = "Bireysel Müşteri";
            Bireysel.musteriTipId = 1;
            musteriTipleri.Add(Bireysel);
            musteriTip Kurumsal = new musteriTip();
            Kurumsal.musteriTipi = "Kurumsal Müşteri";
            Kurumsal.musteriTipId = 2;
            musteriTipleri.Add(Kurumsal);

            //Hesap Tipi
            HesapTip Vadeli = new HesapTip();
            Vadeli.hesapTipId = 1;
            Vadeli.hesapTipi = "Vadeli Hesap";
            hesapTipleri.Add(Vadeli);
            HesapTip Vadesiz = new HesapTip();
            Vadesiz.hesapTipId = 2;
            Vadesiz.hesapTipi = "Vadesiz Hesap";
            hesapTipleri.Add(Vadesiz);
            HesapTip dovizHesap = new HesapTip();
            dovizHesap.hesapTipId = 3;
            dovizHesap.hesapTipi = "Döviz Hesap";
            hesapTipleri.Add(dovizHesap);

            //Kart Tipleri
            KartTipleri krediKarti = new KartTipleri();
            krediKarti.kartTipId = 1;
            krediKarti.kartTipAd = "Kredi Kartı";
            KartTipleri bankaKarti = new KartTipleri();
            bankaKarti.kartTipId = 2;
            bankaKarti.kartTipAd = "Banka Kartı";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            defaultBilgileriDoldur(); 
            
        }

        private void sehirlerComboBx_SelectedIndexChanged(object sender, EventArgs e)
        {
            IlcelerComboBx.DisplayMember = "IlceAdi";
            IlcelerComboBx.ValueMember = "IlceId";
            IlcelerComboBx.DataSource = adresBilgileri.ilceleriDondur(Convert.ToInt32(sehirlerComboBx.SelectedValue.ToString()));
        }
    }
}
