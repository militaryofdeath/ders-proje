﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Banka
    {
        public int bankaId { get; set; }
        public string bankaAdi { get; set; }
        public Adres bankaAdresBilgisi { get; set; }
    }
}
