﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Sube
    {
        public int subeId { get; set; }
        public int bankaId { get; set; }
        public string subeAdi { get; set; }
        public Adres subeAdresBilgisi { get; set; }

    }
}
