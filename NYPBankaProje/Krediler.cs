﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Krediler
    {
        public int krediId { get; set; }
        public int krediTipId { get; set; }
        public int musteriId { get; set; }
        public decimal miktar { get; set; }
        public decimal faiz { get; set; }
        public int vade { get; set; }
        public DateTime krediTarihi = DateTime.Now;
        public DateTime ilkOdemeTarihi { get; set; }
    }
}
