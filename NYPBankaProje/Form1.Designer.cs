﻿namespace NYPBankaProje
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sehirlerComboBx = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IlcelerComboBx = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // sehirlerComboBx
            // 
            this.sehirlerComboBx.FormattingEnabled = true;
            this.sehirlerComboBx.Location = new System.Drawing.Point(55, 7);
            this.sehirlerComboBx.Name = "sehirlerComboBx";
            this.sehirlerComboBx.Size = new System.Drawing.Size(121, 21);
            this.sehirlerComboBx.TabIndex = 0;
            this.sehirlerComboBx.SelectedIndexChanged += new System.EventHandler(this.sehirlerComboBx_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Şehir :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "İlçe :";
            // 
            // IlcelerComboBx
            // 
            this.IlcelerComboBx.FormattingEnabled = true;
            this.IlcelerComboBx.Location = new System.Drawing.Point(55, 34);
            this.IlcelerComboBx.Name = "IlcelerComboBx";
            this.IlcelerComboBx.Size = new System.Drawing.Size(121, 21);
            this.IlcelerComboBx.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IlcelerComboBx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sehirlerComboBx);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox sehirlerComboBx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox IlcelerComboBx;
    }
}

