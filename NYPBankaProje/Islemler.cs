﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Islemler
    {
        public int islemId { get; set; }
        public DateTime islemTarihi = DateTime.Now;
        public int islemTipId { get; set; }
        public int personelId { get; set; }
        public int kimden { get; set; }
        public int kime { get; set; }
        public decimal miktar { get; set; }
    }
}
