﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Kartlar
    {
        public int kartId { get; set; }
        public long kartNumarasi { get; set; }
        public int sifre { get; set; }
        public int kartTipi { get; set; }
        public int musteriId { get; set; }
        public DateTime sonKullanmaTarihi { get; set; }
    }
}
