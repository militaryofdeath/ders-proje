﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Hesap
    {
        public int hesapId { get; set; }
        public long hesapNo { get; set; }
        public decimal bakiye { get; set; }
        public int hesapTipId { get; set; }
        public int musteriId { get; set; }
        public int eksiLimit { get; set; }
    }
}
