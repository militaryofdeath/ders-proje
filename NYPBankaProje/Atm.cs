﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Atm
    {
        public int atmId { get; set; }
        public Adres atmAdresBilgisi { get; set; }
        public decimal Bakiye { get; set; }
        public int personelId { get; set; }
        public int subeId { get; set; }
    
    }
}
