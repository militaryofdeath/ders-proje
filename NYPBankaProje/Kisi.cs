﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Kisi
    {
        public int KisiId { get; set; }
        public string TCKimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public DateTime dogumTarihi { get; set; }
        public DateTime kayitTarihi { get; set; }
        public Adres kisiAdresBilgisi { get; set; }
    }
}
