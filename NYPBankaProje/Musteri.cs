﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Musteri : Kisi
    {
        public int musteriId { get; set; }
        public long musteriNo { get; set; }
        public int musteriTipId { get; set; }
    }
}
