﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Adres
    {
        public int AdresId { get; set; }
        public string Sokak { get; set; }
        public string Mahalle { get; set; }
        public Ilce ilcesi { get; set; }
        public string Telefon { get; set; }

        public List<Ilce> ilceler { get; set; }
        public List<Il> iller { get; set; }
        public Adres()
        {
            iller = new List<Il>();ilceler = new List<Ilce>();
            Il yeniIl = new Il();
            yeniIl.IlAdi = "İzmir";
            yeniIl.IlId = 35;
            iller.Add(yeniIl);
            yeniIl = new Il();
            yeniIl.IlAdi = "Denizli";
            yeniIl.IlId = 20;
            iller.Add(yeniIl);
            Ilce yeniIlce = new Ilce();
            yeniIlce.IlceId = 1;
            yeniIlce.IlceAdi = "Bornova";
            yeniIlce.IlId = 35;
            ilceler.Add(yeniIlce);
            yeniIlce = new Ilce();
            yeniIlce.IlceId = 2;
            yeniIlce.IlceAdi = "Buca";
            yeniIlce.IlId = 35;
            ilceler.Add(yeniIlce);
            yeniIlce = new Ilce();
            yeniIlce.IlceId = 3;
            yeniIlce.IlceAdi = "Karşıyaka";
            yeniIlce.IlId = 35;
            ilceler.Add(yeniIlce);
            yeniIlce = new Ilce();
            yeniIlce.IlceId = 1;
            yeniIlce.IlceAdi = "Pamukkale";
            yeniIlce.IlId = 20;
            ilceler.Add(yeniIlce);
            yeniIlce = new Ilce();
            yeniIlce.IlceId = 2;
            yeniIlce.IlceAdi = "Merkezefendi";
            yeniIlce.IlId = 20;
            ilceler.Add(yeniIlce);
            yeniIlce = new Ilce();
            yeniIlce.IlceId = 3;
            yeniIlce.IlceAdi = "Honaz";
            yeniIlce.IlId = 20;
            
        } //Constructor'da il ve ilçeleri ekliyoruz
        public List<Il> illeriDondur()
        {
            return iller;
        }
        public List<Ilce> ilceleriDondur(int ilId)
        {
            List<Ilce> ilceler2 = new List<Ilce>();
            ilceler2 = ilceler.Where(a => a.IlId == ilId).ToList();
            return ilceler.Where(a => a.IlId == ilId).ToList();
        }
 
      
    }
}
