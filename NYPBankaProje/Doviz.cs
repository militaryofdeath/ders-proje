﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NYPBankaProje
{
    public class Doviz
    {
        
        public int DovizId { get; set; }
        public string DovizAdi { get; set; }
        public decimal tlKarsiligi { get; private set; }
        public decimal alisSatisFarki { get; set; }
        public Doviz()
        {

        }
    }
}
